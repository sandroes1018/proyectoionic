import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegistrarUsuariosPage } from './registrar-usuarios.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrarUsuariosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegistrarUsuariosPage]
})
export class RegistrarUsuariosPageModule {}
